#pragma	once

#if defined(__linux__)						// If we are using linux.
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <SDKDDKVer.h>

//Using SDL, SDL OpenGL, GLEW, standard IO, and strings
#include <SDL.h>
#include <gl\glew.h>
#include <SDL_opengl.h>
#include <gl\glu.h>
#endif

#define GLM_FORCE_RADIANS //Using degrees with glm is deprecated.
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Camera.h"
#include "ModelHandler.h"
#include "ShaderHandler.h"
#include "LightHandler.h"

#include <iostream>

class ScreenObject 
{
public:
	ScreenObject() {}; // For temp objects
	ScreenObject(Model* const model, const glm::vec3& position, const glm::vec3& hitbox, const glm::vec3& scale = glm::vec3(1.0f));

	virtual void draw(Camera* const camera, ShaderHandler::ShaderProgram* const shaderProgram, LightHandler* const lightHandler);

	glm::mat4 getModelMatrix() 			{ return modelMatrix_; };
	glm::vec3 getHitbox() 				{ return hitbox_; };
	glm::vec3* const getPosition() 		{ return &position_; };

	void setModel(Model* const model) 	{ model_ = model; };

protected:
	glm::mat4 modelMatrix_;
	glm::vec3 position_;
	glm::vec3 hitbox_;
	Model* model_;
};