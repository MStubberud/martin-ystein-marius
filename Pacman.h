#pragma once

#include <vector>
#include "Movable.h"
#include "Level.h"

class Pacman : public Movable
{
public:
	Pacman(Model* const model, Model* const model2, const glm::vec3& position, const glm::vec3& hitbox, const glm::vec3& scale = glm::vec3(1.0f), float rotation = 0.0f);
	
	void move(Level* const currentLevel, float deltaTime);
	void draw(Camera* camera, ShaderHandler::ShaderProgram* shaderProgram, LightHandler* lightHandler) override;
	void swapModels();

	void setDirection(Movable::direction direction);
	void setDirectionFirstPerson(Movable::direction direction);
	void toggleFirstPerson() { firstPerson_ = !firstPerson_; };

private:
	void handleCollision(Level* const currentLevel);
	bool checkCollisionWith(ScreenObject* const collidable);
	void resolveCollision(Level* const currentLevel, ScreenObject* const collidable);
	void resolveCollisionFirstPerson(Level* const currentLevel, ScreenObject* const collidable);

	
	std::vector<Model> pacModels_;
	int score_;
	bool firstPerson_;
	
	float cherryTimeLeft_;
	bool activeCherry_;
};