#version 330 core
struct Material {
    sampler2D texture;    
    float shininess;
}; 

struct PointLight {
    vec3 position;
    
    float constant;
    float linear;
    float quadratic;
	
    vec3 diffuse;
    vec3 specular;
};

struct SpotLight {
    vec3 position;
    vec3 direction;
    float cutOff;
    float outerCutOff;
    
    float constant;
    float linear;
    float quadratic;
    
    vec3 diffuse;
    vec3 specular;       
};

#define NR_OF_POINTLIGHTS 5

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;

out vec4 color;

uniform vec3 cameraPos;
uniform vec3 lightPos;

uniform PointLight pointLights[NR_OF_POINTLIGHTS];
uniform SpotLight spotLight;

uniform Material material;
uniform samplerCube cubeMap;

float farPlane = 40;

// array of offset direction for sampling
vec3 gridSamplingDisk[20] = vec3[]
(
   vec3(1, 1, 1), vec3(1, -1, 1), vec3(-1, -1, 1), vec3(-1, 1, 1), 
   vec3(1, 1, -1), vec3(1, -1, -1), vec3(-1, -1, -1), vec3(-1, 1, -1),
   vec3(1, 1, 0), vec3(1, -1, 0), vec3(-1, -1, 0), vec3(-1, 1, 0),
   vec3(1, 0, 1), vec3(-1, 0, 1), vec3(1, 0, -1), vec3(-1, 0, -1),
   vec3(0, 1, 1), vec3(0, -1, 1), vec3(0, -1, -1), vec3(0, 1, -1)
);

// Function prototypes
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir);
vec3 CalcPointLight2(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir);
float ShadowCalculations(vec3 fragPos);

void main()
{    
    // Properties
    vec3 norm = normalize(Normal);
    vec3 viewDir = normalize(cameraPos - FragPos);

    vec3 result = vec3(0, 0, 0);

    result = CalcPointLight2(pointLights[0], norm, FragPos, viewDir);

    for(int i = 1; i < NR_OF_POINTLIGHTS; ++i)
    {
        result += CalcPointLight(pointLights[i], norm, FragPos, viewDir);
    }

    color = vec4(result, 1.0);
}

// Calculates the color when using a point light.
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
    vec3 lightDir = normalize(light.position - fragPos);
    float diff = max(dot(normal, lightDir), 0.0);
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    
    // Attenuation
    float distance = length(light.position - fragPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance)); 
    
    // Combine results
    vec3 diffuse = light.diffuse * diff * vec3(texture(material.texture, TexCoords));
    vec3 specular = light.specular * spec * vec3(texture(material.texture, TexCoords));
    diffuse *= attenuation;
    specular *= attenuation;

    // Calculate shadow
    //float shadow = shadows ? ShadowCalculations(FragPos) : 0.0;
    //return (1.0 - shadow) * (diffuse + specular);
    return (diffuse + specular);
}

vec3 CalcPointLight2(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
    vec3 lightDir = normalize(light.position - fragPos);
    float diff = max(dot(normal, lightDir), 0.0);
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    
    // Attenuation
    float distance = length(light.position - fragPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance)); 
    
    // Combine results
    vec3 diffuse = light.diffuse * diff * vec3(texture(material.texture, TexCoords));
    vec3 specular = light.specular * spec * vec3(texture(material.texture, TexCoords));
    diffuse *= attenuation;
    specular *= attenuation;

    // Calculate shadow
    float shadow = ShadowCalculations(FragPos);
    return (1.0 - shadow) * (diffuse + specular);
    //return (diffuse + specular);
}

float ShadowCalculations(vec3 fragPos)
{
    // Get vector between fragment position and light position
    vec3 fragToLight = fragPos - lightPos;
    // Use the fragment to light vector to sample from the depth map    
    float closestDepth = texture(cubeMap, fragToLight).r;
    // It is currently in linear range between [0,1]. Let's re-transform it back to original depth value
    closestDepth *= farPlane;
    // Now get current linear depth as the length between the fragment and light position
    float currentDepth = length(fragToLight);
    // Now test for shadows
    float bias = 0.05; // We use a much larger bias since depth is now in [nearPlane, far_plane] range
    float shadow = currentDepth -  bias > closestDepth ? 1.0 : 0.0;

    return shadow;
}