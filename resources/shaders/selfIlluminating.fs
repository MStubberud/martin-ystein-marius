#version 330 core
struct Material 
{
	sampler2D texture;
    float shininess;
}; 

struct PointLight 
{
    vec3 position;
    
    float constant;
    float linear;
    float quadratic;
	
    vec3 diffuse;
    vec3 specular;
};

struct SpotLight 
{
    vec3 position;
    vec3 direction;
    float cutOff;
    float outerCutOff;
    
    float constant;
    float linear;
    float quadratic;
    
    vec3 diffuse;
    vec3 specular;       
};

#define NR_OF_POINTLIGHTS 5

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;

out vec4 color;

uniform vec3 cameraPos;
uniform PointLight pointLights[NR_OF_POINTLIGHTS];
uniform SpotLight spotLight;
uniform Material material;
uniform int pointLightIndex;

// Function prototypes
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir);

void main()
{    
    // Properties
    vec3 norm = normalize(Normal);
    vec3 viewDir = normalize(cameraPos - FragPos);

    color = vec4(CalcPointLight(pointLights[pointLightIndex], norm, FragPos, viewDir), 1.0);
}

// Calculates the color when using a point light.
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
    vec3 lightDir = normalize(light.position - fragPos);
    vec3 reflectDir = reflect(lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    
    // Attenuation
    float distance = length(light.position - fragPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance)); 
    
    // Combine results
    vec3 diffuse = light.diffuse * vec3(texture(material.texture, TexCoords));
    vec3 specular = light.specular * spec * vec3(texture(material.texture, TexCoords));
    diffuse *= attenuation;
    specular *= attenuation;
    return diffuse + specular;
}