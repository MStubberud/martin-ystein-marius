#include "Camera.h"
#include "WindowHandler.h"

Camera::Camera(glm::vec3* const position, glm::vec3* const front)
{
	up_ = glm::vec3(0.0f, 1.0f, 0.0f);
	offset_ = glm::vec3(0.0f, 0.0f, 0.0f);
	position_ = position;
	front_ = front;
	
	perspectiveView_ = true;
	firstPersonView_ = true;

	setProjection(0.1f, 200.0f);
}

Camera::Camera(const glm::ivec2& mapDimensions)
{
	glm::vec3 mapCenter =glm::vec3(mapDimensions.x, 0.0f, mapDimensions.y);
	
	up_ = glm::vec3(0.0f, 1.0f, 0.0f);
	offset_ = glm::vec3(0.0f, glm::sin(glm::radians(60.0f)) * mapDimensions.y * 2.0f, glm::cos(glm::radians(60.0f)) * mapDimensions.y * 2.0f);
	position_ = new glm::vec3(offset_ + mapCenter);
	front_ = new glm::vec3(glm::normalize(-offset_));
	
	perspectiveView_ = true;
	firstPersonView_ = false;

	setProjection(0.1f, 200.0f);
}

Camera::~Camera()
{
	// Offset is set if the camera got its own pos and not getting it from somewhere else
	if (offset_.x != 0 || offset_.y != 0 || offset_.z != 0)
	{
		delete position_;
		delete front_;
	}
}

// Creates perspective projection matrix
void Camera::setProjection(float near, float far, float fov) 
{
	projectionMatrix_ =  glm::perspective(fov, WindowHandler::getInstance().getScreenSize().x / WindowHandler::getInstance().getScreenSize().y, near, far);
}

// Creates the frustum
void Camera::setOrthographic(float near, float far)
{
	projectionMatrix_ =  glm::ortho(-50.0f, 50.f, -30.0f, 30.0f, near, far);
}

glm::mat4 Camera::getViewMatrix() 
{
	return glm::lookAt(*position_, *front_ + *position_, up_);
}

void Camera::togglePerspectiveView()
{
	if (!firstPersonView_)
	{
		perspectiveView_ = !perspectiveView_;
		if (perspectiveView_)
		{
			setProjection(0.1f, 200.0f);
		}
		else
		{
			setOrthographic(0.1f, 200.0f);
		}
	}
}