#pragma	once

#include <string>
#include <vector>
#include <glm/glm.hpp>

#include "ScreenObject.h"
#include "ModelHandler.h"
#include "GhostHandler.h"
#include "Collectible.h"
#include "Portal.h"

class Level 
{
public:
	Level(std::string filepath, ModelHandler* modelHandler);

	void updateGhosts(float deltaTime, glm::vec3* pacmanPos);
	
	std::vector<ScreenObject>* getWalls() { return &walls_; };
	std::vector<Portal>* getPortals() { return &portals_; };
	std::vector<Collectible>* getOrbs() { return &orbs_; };
	std::vector<Collectible>* getCherries() { return &cherries_; };
	ScreenObject* getFloor() { return &floor_; };
	GhostHandler* getGhosts() { return &ghostHandler_; };

	const glm::ivec2& getMapDimensions() { return mapDimensions_; };
	const glm::vec3& getStartingPosition() { return startingPosition_; };

private:
	void loadMap(std::string filepath); //Loads map data from file
	void createMap(ModelHandler* modelHandler);	//This should create a ScreenObject for each wall segment.  
	
	std::vector<std::vector<int>> map_;

	std::vector<ScreenObject> walls_;
	std::vector<Portal> portals_;
	std::vector<Collectible> orbs_;
	std::vector<Collectible> cherries_;
	ScreenObject floor_;

	GhostHandler ghostHandler_;
	
	// Position given to pacman at the start of a level
	glm::vec3 startingPosition_;

	glm::ivec2 mapDimensions_;
};