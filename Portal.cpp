#include "Portal.h"

Portal::Portal(Model* model, glm::vec3 position, glm::vec3 hitbox, glm::vec3 scale, glm::ivec2 mapDimensions)
				: ScreenObject(model, position, hitbox, scale), camera_(&cameraPosition_, &cameraFront_)
{
	resolution = 600;
    glGenFramebuffers(1, &textureFBO_);
    glBindFramebuffer(GL_FRAMEBUFFER, textureFBO_);  

    // Create a color attachment texture
    glGenTextures(1, &textureId_);
    glBindTexture(GL_TEXTURE_2D, textureId_);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, resolution, resolution, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureId_, 0);
    // Create a renderbuffer object for depth and stencil attachment (we won't be sampling these)
    glGenRenderbuffers(1, &RBO_);
    glBindRenderbuffer(GL_RENDERBUFFER, RBO_); 
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, resolution, resolution); // Use a single renderbuffer object for both a depth AND stencil buffer.
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, RBO_); // Now actually attach it

    // Now that we actually created the framebuffer and added all attachments we want to check if it is actually complete now
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!" <<std::endl;

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

	if(position.x > mapDimensions.x - 1)
	{
		cameraPosition_ = position - glm::vec3(mapDimensions.x * 2 - 2, 0.0f, 0.0f);
		cameraFront_ = glm::vec3(1.0f, 0.0f, 0.0f);
	}		
	else
	{
		cameraPosition_ = position + glm::vec3(mapDimensions.x * 2 - 2, 0.0f, 0.0f);
		cameraFront_ = glm::vec3(-1.0f, 0.0f, 0.0f);
	}
	camera_.setProjection(glm::perspective(glm::radians(70.f), 1.f, 0.1f, 200.f));
}

Portal::~Portal()
{
	glDeleteFramebuffers(1, &textureFBO_);
}

void Portal::setUpFBO()
{
	glBindFramebuffer(GL_FRAMEBUFFER, textureFBO_);
	glViewport(0, 0, resolution, resolution);
	//glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glEnable(GL_DEPTH_TEST);
}

void Portal::draw(Camera* const camera, ShaderHandler::ShaderProgram* const shaderProgram, LightHandler* const lightHandler)
{
	model_->sendTextureId(textureId_);

	ScreenObject::draw(camera, shaderProgram, lightHandler);
}