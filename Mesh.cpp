#include "Mesh.h"

Mesh::Mesh(const std::vector<Vertex>& vertices, const std::vector<GLuint>& indices, const Texture& texture)
{
	vertices_ = vertices;
	indices_ = indices;
	texture_ = texture;
	shininess_ = 1.0f;

	setupMesh();
}

void Mesh::draw(Camera* const camera, ShaderHandler::ShaderProgram* const shaderProgram, LightHandler* const lightHandler, glm::mat4* const modelMatrix)
{
	glm::mat4 MVPMatrix = camera->getProjectionMatrix() * camera->getViewMatrix() * (*modelMatrix);

	PointLight* pacmanLight 		= lightHandler->getPointLight(PACMAN);
	PointLight* redGhostLight		= lightHandler->getPointLight(RED_GHOST);
	PointLight* blueGhostLight		= lightHandler->getPointLight(BLUE_GHOST);
	PointLight* greenGhostLight		= lightHandler->getPointLight(GREEN_GHOST);
	PointLight* yellowGhostLight	= lightHandler->getPointLight(YELLOW_GHOST);
	SpotLight* spotLight 			= lightHandler->getSpotLight();

	//Bind program
	glUseProgram(shaderProgram->programId);

	//Bind Cube
	glBindVertexArray(VAO_);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO_);

	// Send our transformation to the currently bound shader, 
	// in the "MVP" uniform
	glUniformMatrix4fv(shaderProgram->MVPId, 1, GL_FALSE, glm::value_ptr(MVPMatrix));		//Note that glm::value_ptr(gMVPMatrix) is the same as &gMVPMatrix[0][0]]
	glUniformMatrix4fv(shaderProgram->modelMatrixId, 1, GL_FALSE, glm::value_ptr(*modelMatrix));

	//Send in light and color information
	glUniform3f(shaderProgram->cameraPositionId, camera->getPosition().x, camera->getPosition().y, camera->getPosition().z);

	glUniform1i(shaderProgram->pointLightIndexId, lightHandler->pointLightIndex);
	glUniform3f(shaderProgram->lightPositionId, lightHandler->getCamera()->getPosition().x, lightHandler->getCamera()->getPosition().y, lightHandler->getCamera()->getPosition().z);

	int i = 0;
	for(auto &s : (*lightHandler->getShadowTransforms()))
	{
		glUniformMatrix4fv(shaderProgram->shadowMatricesIds[i], 1, GL_FALSE, glm::value_ptr(s));
		++i;
	}
	
	glUniform1f(shaderProgram->shininessId, shininess_);

	glUniform3f(shaderProgram->pacmanLightPositionId, 		pacmanLight->position->x, 			pacmanLight->position->y, 			pacmanLight->position->z);
	glUniform1f(shaderProgram->pacmanLightConstantId, 		pacmanLight->constant);
	glUniform1f(shaderProgram->pacmanLightLinearId, 		pacmanLight->linear);
	glUniform1f(shaderProgram->pacmanLightQuadraticId, 		pacmanLight->quadratic);
	glUniform3f(shaderProgram->pacmanLightDiffuseId, 		pacmanLight->diffuseColor.x, 		pacmanLight->diffuseColor.y, 		pacmanLight->diffuseColor.z);
	glUniform3f(shaderProgram->pacmanLightSpecularId, 		pacmanLight->specularColor.x, 		pacmanLight->specularColor.y, 		pacmanLight->specularColor.z);

	glUniform3f(shaderProgram->redGhostLightPositionId, 	redGhostLight->position->x, 		redGhostLight->position->y, 		redGhostLight->position->z);
	glUniform1f(shaderProgram->redGhostLightConstantId, 	redGhostLight->constant);
	glUniform1f(shaderProgram->redGhostLightLinearId, 		redGhostLight->linear);
	glUniform1f(shaderProgram->redGhostLightQuadraticId, 	redGhostLight->quadratic);
	glUniform3f(shaderProgram->redGhostLightDiffuseId, 		redGhostLight->diffuseColor.x, 		redGhostLight->diffuseColor.y, 		redGhostLight->diffuseColor.z);
	glUniform3f(shaderProgram->redGhostLightSpecularId, 	redGhostLight->specularColor.x, 	redGhostLight->specularColor.y, 	redGhostLight->specularColor.z);
	
	glUniform3f(shaderProgram->blueGhostLightPositionId, 	blueGhostLight->position->x, 		blueGhostLight->position->y, 		blueGhostLight->position->z);
	glUniform1f(shaderProgram->blueGhostLightConstantId, 	blueGhostLight->constant);
	glUniform1f(shaderProgram->blueGhostLightLinearId, 		blueGhostLight->linear);
	glUniform1f(shaderProgram->blueGhostLightQuadraticId, 	blueGhostLight->quadratic);
	glUniform3f(shaderProgram->blueGhostLightDiffuseId, 	blueGhostLight->diffuseColor.x, 	blueGhostLight->diffuseColor.y, 	blueGhostLight->diffuseColor.z);
	glUniform3f(shaderProgram->blueGhostLightSpecularId, 	blueGhostLight->specularColor.x, 	blueGhostLight->specularColor.y, 	blueGhostLight->specularColor.z);
	
	glUniform3f(shaderProgram->greenGhostLightPositionId, 	greenGhostLight->position->x, 		greenGhostLight->position->y, 		greenGhostLight->position->z);
	glUniform1f(shaderProgram->greenGhostLightConstantId, 	greenGhostLight->constant);
	glUniform1f(shaderProgram->greenGhostLightLinearId, 	greenGhostLight->linear);
	glUniform1f(shaderProgram->greenGhostLightQuadraticId, 	greenGhostLight->quadratic);
	glUniform3f(shaderProgram->greenGhostLightDiffuseId, 	greenGhostLight->diffuseColor.x, 	greenGhostLight->diffuseColor.y, 	greenGhostLight->diffuseColor.z);
	glUniform3f(shaderProgram->greenGhostLightSpecularId, 	greenGhostLight->specularColor.x, 	greenGhostLight->specularColor.y, 	greenGhostLight->specularColor.z);

	glUniform3f(shaderProgram->yellowGhostLightPositionId, 	yellowGhostLight->position->x, 		yellowGhostLight->position->y, 		yellowGhostLight->position->z);
	glUniform1f(shaderProgram->yellowGhostLightConstantId, 	yellowGhostLight->constant);
	glUniform1f(shaderProgram->yellowGhostLightLinearId, 	yellowGhostLight->linear);
	glUniform1f(shaderProgram->yellowGhostLightQuadraticId, yellowGhostLight->quadratic);
	glUniform3f(shaderProgram->yellowGhostLightDiffuseId, 	yellowGhostLight->diffuseColor.x, 	yellowGhostLight->diffuseColor.y, 	yellowGhostLight->diffuseColor.z);
	glUniform3f(shaderProgram->yellowGhostLightSpecularId, 	yellowGhostLight->specularColor.x, 	yellowGhostLight->specularColor.y, 	yellowGhostLight->specularColor.z);

	glUniform3f(shaderProgram->spotLightPositionId, 	spotLight->position->x, 	spotLight->position->y, 	spotLight->position->z);
	glUniform3f(shaderProgram->spotLightDirectionId, 	spotLight->direction->x, 	spotLight->direction->y, 	spotLight->direction->z);
	glUniform1f(shaderProgram->spotLightCutOffId, 		spotLight->cutOff);
	glUniform1f(shaderProgram->spotLightOuterCutOffId, 	spotLight->outerCutOff);
	glUniform1f(shaderProgram->spotLightConstantId, 	spotLight->constant);
	glUniform1f(shaderProgram->spotLightLinearId, 		spotLight->linear);
	glUniform1f(shaderProgram->spotLightQuadraticId, 	spotLight->quadratic);
	glUniform3f(shaderProgram->spotLightDiffuseId, 		spotLight->diffuseColor.x, 	spotLight->diffuseColor.y, 	spotLight->diffuseColor.z);
	glUniform3f(shaderProgram->spotLightSpecularId, 	spotLight->specularColor.x, spotLight->specularColor.y, spotLight->specularColor.z);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture_.id);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_CUBE_MAP, *lightHandler->getCubeMap());
	glUniform1i(shaderProgram->textureId, 0);
	glUniform1i(shaderProgram->cubeMapId, 1);

	//Draw Cube
	glDrawElements(GL_TRIANGLES, indices_.size(), GL_UNSIGNED_INT, NULL);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	//Unbind program
	glUseProgram(0);
}

void Mesh::setupMesh()
{
	glGenVertexArrays(1, &VAO_);
	glGenBuffers(1, &VBO_);
	glGenBuffers(1, &IBO_);

	glBindVertexArray(VAO_);
	
	glBindBuffer(GL_ARRAY_BUFFER, VBO_);
	glBufferData(GL_ARRAY_BUFFER, vertices_.size() * sizeof(Vertex), &vertices_[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO_);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices_.size() * sizeof(GLuint), &indices_[0], GL_STATIC_DRAW);

	// Vertex positions
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);

	// Vertex normals
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, normal));

	// Vertex texture coordinates
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, texCoords));	

	glBindVertexArray(0);
}
