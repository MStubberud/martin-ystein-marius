#pragma once


#include <unordered_map>
#include <memory>

#if defined(__linux__)						// If we are using linux.

#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <SDKDDKVer.h>

//Using SDL, SDL OpenGL, GLEW, standard IO, and strings
#include <SDL.h>
#include <gl\glew.h>
#include <SDL_opengl.h>
#include <gl\glu.h>
#endif

#include "shader.h"

#include <string>


// Handles loading multiple shaderprogram and switching between them.
class ShaderHandler
{
public:
	class ShaderProgram
	{
	public:
		ShaderProgram(const std::string& shader);
		ShaderProgram(const std::string& shader, bool t);
		~ShaderProgram();

		GLuint programId;
		GLuint MVPId;
		GLuint modelMatrixId;

		GLuint textureId;
		GLuint shininessId;

		GLuint cubeMapId;
		GLuint shadowMatricesIds[6];

		GLuint pacmanLightPositionId;
		GLuint pacmanLightConstantId;
		GLuint pacmanLightLinearId;
		GLuint pacmanLightQuadraticId;
		GLuint pacmanLightDiffuseId;
		GLuint pacmanLightSpecularId;

		GLuint redGhostLightPositionId;
		GLuint redGhostLightConstantId;
		GLuint redGhostLightLinearId;
		GLuint redGhostLightQuadraticId;
		GLuint redGhostLightDiffuseId;
		GLuint redGhostLightSpecularId;

		GLuint blueGhostLightPositionId;
		GLuint blueGhostLightConstantId;
		GLuint blueGhostLightLinearId;
		GLuint blueGhostLightQuadraticId;
		GLuint blueGhostLightDiffuseId;
		GLuint blueGhostLightSpecularId;

		GLuint greenGhostLightPositionId;
		GLuint greenGhostLightConstantId;
		GLuint greenGhostLightLinearId;
		GLuint greenGhostLightQuadraticId;
		GLuint greenGhostLightDiffuseId;
		GLuint greenGhostLightSpecularId;

		GLuint yellowGhostLightPositionId;
		GLuint yellowGhostLightConstantId;
		GLuint yellowGhostLightLinearId;
		GLuint yellowGhostLightQuadraticId;
		GLuint yellowGhostLightDiffuseId;
		GLuint yellowGhostLightSpecularId;

		GLuint spotLightPositionId;
		GLuint spotLightDirectionId;
		GLuint spotLightCutOffId;
		GLuint spotLightOuterCutOffId;
		GLuint spotLightConstantId;
		GLuint spotLightLinearId;
		GLuint spotLightQuadraticId;
		GLuint spotLightDiffuseId;
		GLuint spotLightSpecularId;

		GLuint cameraPositionId;
		GLuint lightPositionId;
		GLuint pointLightIndexId;
	};

	ShaderProgram* initializeShaders();


	ShaderProgram* getShader(const std::string& shader);

	ShaderProgram* setShader(const std::string& shader);

private:

	std::unordered_map<std::string, std::unique_ptr<ShaderProgram>> shaders;
};

