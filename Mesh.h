#pragma once
#if defined(__linux__)						// If we are using linux.
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <SDKDDKVer.h>

//Using SDL, SDL OpenGL, GLEW, standard IO, and strings
#include <SDL.h>
#include <gl\glew.h>
#include <SDL_opengl.h>
#include <gl\glu.h>
#endif

#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <string>
#include <sstream>
#include <vector>
#include <cstddef>

#include "Camera.h"
#include "ShaderHandler.h"
#include "LightHandler.h"

struct Vertex
{
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 texCoords;
};

struct Texture
{
	GLuint id;
	std::string type;
	aiString path;
};

class Mesh
{
public:
	Mesh(const std::vector<Vertex>& vertices, const std::vector<GLuint>& indices, const Texture& texture);
	void draw(Camera* const camera, ShaderHandler::ShaderProgram* const shaderProgram, LightHandler* const lightHandler, glm::mat4* const modelMatrix);

	void setTextureId(GLuint texId) { texture_.id = texId; };

	std::vector<Vertex> vertices_;
	std::vector<GLuint> indices_;
	Texture texture_;

private:
	void setupMesh();
	
	GLuint VAO_; 
	GLuint VBO_;
	GLuint IBO_;
	float shininess_;
};