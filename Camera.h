#pragma once

#define GLM_FORCE_RADIANS //Using degrees with glm is deprecated.
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/glew.h>
#include <iostream>
#include "InputHandler.h"

class Camera 
{
public:
	Camera(glm::vec3* const position, glm::vec3* const front);
	Camera(const glm::ivec2& mapDimensions);
	~Camera();
	
	void setProjection(const glm::mat4& projectionMatrix) { projectionMatrix_ = projectionMatrix;};
	void setProjection(float near, float far, float fov = glm::radians(70.f));
	void setOrthographic(float near, float far);

	void lookAt(glm::vec3* const lookAtPos) { lookAtPos_ = lookAtPos; };
	void togglePerspectiveView();

	glm::mat4 getViewMatrix();
	glm::mat4 getProjectionMatrix() { return projectionMatrix_; };
	glm::vec3 getPosition() { return *position_; };
	bool isFirstPerson() { return firstPersonView_; };

private:
	glm::mat4 projectionMatrix_;

	glm::vec3* position_;
	glm::vec3* front_;
	glm::vec3 up_;
	glm::vec3 offset_;
	glm::vec3* lookAtPos_;

	bool perspectiveView_;
	bool firstPersonView_;
};