#include "Level.h"
#include "WindowHandler.h"

Level::Level(std::string filepath, ModelHandler* modelHandler) 
{
	loadMap(filepath);
	createMap(modelHandler);
}

/* 
Reads a text file containing map data in the following format:
5x5
1 1 1 1 1
1 0 0 0 1
2 0 1 0 0
1 0 0 0 1
1 1 1 1 1
The first line defines the dimensions of the map
The following matrix has 1s representing walls and 0s representing open space.
2 represents Pac Mans start position.
*/
void Level::loadMap(std::string filepath) 
{
	int temp;
	FILE* file = fopen(filepath.c_str(), "r");
	
	fscanf(file, "%dx%d", &mapDimensions_.x, &mapDimensions_.y);

	for(int i = 0; i<mapDimensions_.y; i++) 
	{
		std::vector<int> row;
		for(int j = 0; j<mapDimensions_.x; j++) 
		{
			fscanf(file, " %d", &temp);
			row.push_back(temp);
		}
		map_.push_back(row);
	}

	fclose(file);
	file = nullptr;
}

//Uses the map data to create ScreenObjects that represent the walls of the map.
void Level::createMap(ModelHandler* modelHandler) 
{
	glm::vec2 pos, size;
	int rows = map_.size();
	int cells = 0;
	glm::vec2 doorDir;
	ScreenObject tempScreenObj;
	Collectible tempCollectible;
	portals_.reserve(4);

	for(int i = 0; i<rows; i++) 
	{
		cells = map_[i].size();

		for(int j = 0; j<cells; j++) 
		{
			switch(map_[i][j])
			{
			case 1: 
				tempScreenObj = ScreenObject(modelHandler->getModel("wall"), glm::vec3(j * 2, 1.0f, i * 2), glm::vec3(2.0f));
				
				walls_.push_back(tempScreenObj);
				break;

			case 2:
				startingPosition_ = glm::vec3(j * 2, 1.0f, i * 2);
				break;

			case 3:
				tempCollectible = Collectible(modelHandler->getModel("gift"), glm::vec3(j * 2, 0.6f, i * 2), glm::vec3(0.4f), glm::vec3(0.2f), 90.0f);
				tempCollectible.setScales(glm::vec3(0.2), glm::vec3(0.4));
				orbs_.push_back(tempCollectible);
				break;

			case 5:
				
				if (map_[i][j+1] == 6)
					doorDir = glm::vec2(1,0);
				else if (map_[i][j-1] == 6)
					doorDir = glm::vec2(-1,0);
				else if (map_[i+1][j] == 6)
					doorDir = glm::vec2(0,1);
				else if (map_[i-1][j] == 6)
					doorDir = glm::vec2(0,-1);
					
				ghostHandler_.init(glm::vec2(2.f), glm::vec2(j * 2.0f, i * 2.0f), doorDir, modelHandler);
				break;

			case 7:
				portals_.emplace_back(modelHandler->getModel("portal"), glm::vec3(j * 2, 1.0f, i * 2), glm::vec3(2.0f), glm::vec3(1.0f), mapDimensions_);
				break;
			case 8:
				tempCollectible = Collectible(modelHandler->getModel("santaHat"), glm::vec3(j * 2, 0.6f, i * 2), glm::vec3(0.4f), glm::vec3(0.3f), 90.0f);
				tempCollectible.setScales(glm::vec3(0.1), glm::vec3(0.3));
				cherries_.push_back(tempCollectible);
				break;

			default:
				break;
			}
		}
	}
	
	floor_ = ScreenObject(modelHandler->getModel("floor"), glm::vec3(mapDimensions_.x - 1, -0.01f, mapDimensions_.y - 1), glm::vec3(2.0f), glm::vec3(mapDimensions_.x - 2, 0.01f, mapDimensions_.y - 2));
}

void Level::updateGhosts(float deltaTime, glm::vec3* pacmanPos)
{
	ghostHandler_.update(deltaTime, pacmanPos, map_);
}