#pragma once

#include <glm/glm.hpp>

#include "Model.h"
#include "Movable.h"

class Collectible : public Movable
{
public:
	Collectible() {}; // For temp objects
	Collectible(Model* model, glm::vec3 position, glm::vec3 hitbox, glm::vec3 scale = glm::vec3(1.0f), float rotation = 0.0f)
	: Movable(model, position, hitbox, scale, rotation) {};
	
	void update(float deltaTime);
	void toggleScale();
	void setScales(const glm::vec3& small, const glm::vec3& large);

private:
	glm::vec3 smallScale_;
	glm::vec3 largeScale_;
};