#include "LightHandler.h"

LightHandler::LightHandler() : camera_(spotLight_.position, spotLight_.direction)
{
	for (size_t i = 0; i < NUM_POINTLIGHTS; ++i)
	{
		pointLights_[i].constant = 1.0f;
		pointLights_[i].linear = 0.09f;
		pointLights_[i].quadratic = 0.032f;
		pointLights_[i].diffuseColor = glm::vec3(1.0f);
		pointLights_[i].specularColor = glm::vec3(1.0f);	
	}
	
	spotLight_.cutOff = glm::cos(glm::radians(20.0f));
	spotLight_.outerCutOff = glm::cos(glm::radians(30.0f));
	spotLight_.constant = 1.0f;
	spotLight_.linear = 0.09f;
	spotLight_.quadratic = 0.032f;
	spotLight_.diffuseColor = glm::vec3(1.0f);
	spotLight_.specularColor = glm::vec3(1.0f);

	shadowDepth_ = glm::ivec2(1024, 1024);

	initFBO();
}

LightHandler::~LightHandler()
{
	//glDeleteFramebuffers(1, &FBO_);
}

void LightHandler::initFBO()
{
	glGenFramebuffers(1, &FBO_);
	glBindFramebuffer(GL_FRAMEBUFFER, FBO_);  

	// Create a color attachment texture
	glGenTextures(1, &cubeMap_);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMap_);

	for(GLuint i = 0; i < 6; ++i)
	{
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT, shadowDepth_.x, shadowDepth_.y, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	//glBindTexture(GL_TEXTURE_2D, 0);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, cubeMap_, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
        std::cout << "Framebuffer not complete!" << std::endl;
	}
	//glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    camera_.setProjection(glm::perspective(glm::radians(90.f), 1.f, 0.1f, 20.f));

}

void LightHandler::setUpFBO()
{
	glViewport(0, 0, shadowDepth_.x, shadowDepth_.y);
	glBindFramebuffer(GL_FRAMEBUFFER, FBO_);

	shadowTransforms.resize(0);
	shadowTransforms.push_back(camera_.getViewMatrix() * glm::lookAt(*pointLights_[0].position, *pointLights_[0].position + glm::vec3( 1.0,  0.0,  0.0), glm::vec3(0.0, -1.0,  0.0)));
	shadowTransforms.push_back(camera_.getViewMatrix() * glm::lookAt(*pointLights_[0].position, *pointLights_[0].position + glm::vec3(-1.0,  0.0,  0.0), glm::vec3(0.0, -1.0,  0.0)));
	shadowTransforms.push_back(camera_.getViewMatrix() * glm::lookAt(*pointLights_[0].position, *pointLights_[0].position + glm::vec3( 0.0,  1.0,  0.0), glm::vec3(0.0,  0.0,  1.0)));
	shadowTransforms.push_back(camera_.getViewMatrix() * glm::lookAt(*pointLights_[0].position, *pointLights_[0].position + glm::vec3( 0.0, -1.0,  0.0), glm::vec3(0.0,  0.0, -1.0)));
	shadowTransforms.push_back(camera_.getViewMatrix() * glm::lookAt(*pointLights_[0].position, *pointLights_[0].position + glm::vec3( 0.0,  0.0,  1.0), glm::vec3(0.0, -1.0,  0.0)));
	shadowTransforms.push_back(camera_.getViewMatrix() * glm::lookAt(*pointLights_[0].position, *pointLights_[0].position + glm::vec3( 0.0,  0.0, -1.0), glm::vec3(0.0, -1.0,  0.0)));
}

void LightHandler::setPointLightAt(glm::vec3* const position, const PointLightEnum& index, const glm::vec3& color)
{
	pointLights_[index].position = position;
	pointLights_[index].diffuseColor = color;
	pointLights_[index].specularColor = color;
}

void LightHandler::setSpotlightAt(glm::vec3* const position,const glm::vec3& color)
{
	spotLight_.position = position;
	spotLight_.diffuseColor = color;
	spotLight_.specularColor = color;
	camera_ = Camera(spotLight_.position, spotLight_.direction);
}

