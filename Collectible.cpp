#include "Collectible.h"

void Collectible::update(float deltaTime)
{
	rotation_ += deltaTime;
	rotationDirection_ = glm::vec3(glm::cos(rotation_), 0.0f, glm::sin(rotation_));
}

void Collectible::setScales(const glm::vec3& small, const glm::vec3& large)
{
	smallScale_ = small;
	largeScale_ = large;
	scale_ = largeScale_;
}

void Collectible::toggleScale()
{
	if (scale_ == smallScale_)
	{
		scale_ = largeScale_;
	}
	else
	{
		scale_ = smallScale_;
	}
}