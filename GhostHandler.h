#pragma once

#include <vector>
#include <memory>
#include <random>

#include "ShaderHandler.h"
#include "Movable.h"
#include "ModelHandler.h"

enum ColorOrder
{
	RED,
	BLUE,
	GREEN,
	YELLOW,
	NUM_COLORS
};

class GhostHandler
{
public:
	void init(const glm::vec2 &size, const glm::vec2 &pos, const glm::vec2 &doorDir, ModelHandler* modelHandler);
	void draw(Camera* camera, ShaderHandler::ShaderProgram* shaderProgram, LightHandler* lightHandler);

	void update(float deltaTime, glm::vec3* pacmanPos, const std::vector<std::vector<int>>& map);
	void die(int index) { isDead[index] = true; };
	bool isActive(int index) { return !isDead[index]; };

	void setShader(ShaderHandler::ShaderProgram* const shader);
	glm::vec3* const getGhostPosition(ColorOrder color) {return ghosts[color].getPosition(); };
	std::vector<Movable>* getGhosts() { return &ghosts; };

private:
	// Movement stuff
	glm::vec3 simpleAI(const std::vector<std::vector<int>>& map, glm::vec3& end, const int& xTile, const int& yTile, int index);
	glm::vec3 findTarget(const std::vector<std::vector<int>>& map, glm::vec3& end, const int& xTile, const int& yTile, const int& index, glm::vec3* goal);
	//glm::vec2 fleeTarget(const float& dt, const std::vector<std::vector<int>>& map, glm::vec2& end, const int& xTile, const int& yTile, const int& index, const glm::vec2& goal);
	
	Movable::direction findDirection(const glm::vec3& moveDir);
	bool areClose(const glm::vec3 &pos1, const glm::vec3 &pos2);

	std::vector<Movable> ghosts;
	
	std::default_random_engine generator;
	glm::vec3 startPos;		// Spawnpoint
	glm::vec3 tileSize;		// Size of tiles on screen

	glm::vec3 moveDir[NUM_COLORS];
	glm::vec3 goalPos[NUM_COLORS];
	bool isDead[NUM_COLORS];
};