#pragma once
#if defined(__linux__)						// If we are using linux.
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_image.h>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <SDKDDKVer.h>
#include <SDL_image.h>

//Using SDL, SDL OpenGL, GLEW, standard IO, and strings
#include <SDL.h>
#include <gl\glew.h>
#include <SDL_opengl.h>
#include <gl\glu.h>
#endif

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <string>
#include <vector>
#include <cstddef>
#include <iostream>

#include "Mesh.h"
#include "Camera.h"
#include "ShaderHandler.h"
#include "LightHandler.h"

class Model
{
public:
	Model() {}; // Empty constructor because problems with std::map otherwise in modelhandler
	Model(const std::string &path);

	void draw(Camera* const camera, ShaderHandler::ShaderProgram* const shaderProgram, LightHandler* const lightHandler, glm::mat4* const modelMatrix);
	void sendTextureId(GLuint texId);

private:
	void loadModel(const std::string &path);
	void processNode(aiNode* node, const aiScene* scene);
	Mesh processMesh(aiMesh* mesh, const aiScene* scene);
	GLint textureFromFile(const char* path, std::string directory);
	Texture loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName);
	GLenum getCorrectTexelFormat(const SDL_Surface *textureSurface);

	std::vector<Mesh> meshes_;
	std::string directory_;
};