#pragma once

#include "ScreenObject.h"
#include "Camera.h"

class Portal : public ScreenObject
{
public:
	Portal(Model* model, glm::vec3 position, glm::vec3 hitbox, glm::vec3 scale, glm::ivec2 mapDimensions);
	~Portal();
	
	void setUpFBO();
	void draw(Camera* const camera, ShaderHandler::ShaderProgram* const shaderProgram, LightHandler* const lightHandler);
	Camera* const getCamera() { return &camera_; };

private:
	GLuint textureFBO_;
	GLuint RBO_;
	GLuint textureId_;

	GLint resolution; 
	
	Camera camera_;
	glm::vec3 cameraPosition_;
	glm::vec3 cameraFront_;
};