#pragma once

#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>

#include "Camera.h"
#include "ShaderHandler.h"

enum PointLightEnum 
{
	PACMAN,
	RED_GHOST,
	BLUE_GHOST,
	GREEN_GHOST,
	YELLOW_GHOST,
	NUM_POINTLIGHTS
};

struct PointLight
{
	glm::vec3* position;

	float constant;
	float linear;
	float quadratic;

	glm::vec3 diffuseColor;
	glm::vec3 specularColor;
};

struct SpotLight
{
	glm::vec3* position;
	glm::vec3* direction;
	float cutOff;
	float outerCutOff;	

	float constant;
	float linear;
	float quadratic;
	
	glm::vec3 diffuseColor;
	glm::vec3 specularColor;
};

class LightHandler
{
public:
	LightHandler();
	~LightHandler();

	void initFBO();
	void setUpFBO();

	PointLight* getPointLight(PointLightEnum index) { return &pointLights_[index]; };
	SpotLight* getSpotLight() { return &spotLight_; };
	Camera* getCamera() { return &camera_; };
	ShaderHandler::ShaderProgram* getShader() { return shader_; };
	GLuint* getCubeMap() { return &cubeMap_; };
	std::vector<glm::mat4>* getShadowTransforms() { return &shadowTransforms; };

	void setPointLightAt(glm::vec3* const position, const PointLightEnum& index, const glm::vec3& color = glm::vec3(1.0f));
	void setSpotlightAt(glm::vec3* const position, const glm::vec3& color = glm::vec3(1.0f));
	void setDirection(glm::vec3* direction) 				{ spotLight_.direction = direction; };
	void setShader(ShaderHandler::ShaderProgram* shader) 	{ shader_ = shader; };

	int pointLightIndex; // Used to determine which pointlight to selfIlluminate with

private:
	PointLight pointLights_[NUM_POINTLIGHTS];
	SpotLight spotLight_;
	
	GLuint FBO_;
	GLuint cubeMap_;
	glm::ivec2 shadowDepth_;

	std::vector<glm::mat4> shadowTransforms;

	Camera camera_;
	ShaderHandler::ShaderProgram* shader_;
};