#include "ModelHandler.h"

void ModelHandler::loadModelData(std::string name, const std::string &path) 
{
	models_.insert(std::pair<std::string, Model>(name, Model(path)));
}

Model* ModelHandler::getModel(std::string name) 
{
	if(models_.count(name)) 
	{
		return &models_[name];
	} 

	printf("Error!: No model by name %s could be found.\n", name.c_str());
	return nullptr;
}