#if defined(__linux__)						// If we are using linux.
#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <SDKDDKVer.h>

//Using SDL, SDL OpenGL, GLEW, standard IO, and strings
#include <SDL.h>
#include <gl\glew.h>
#include <SDL_opengl.h>
#include <gl\glu.h>
#endif

#include <stdio.h>
#include <SDL2/SDL.h>
#include <string>
#include <vector>
#include <iostream>

#include "InputHandler.h"
#include "WindowHandler.h"
#include "ShaderHandler.h"
#include "ModelHandler.h"
#include "LightHandler.h"

#include "Level.h"
#include "GameEvent.h"
#include "ScreenObject.h"
#include "Pacman.h"
#include "Camera.h"


//Creates a Level object for each line in gLEVELS_FILE andplace it it the gLevels vector.
//Those lines are paths of files with map data.
//See Level::loadMap for more information.
//Returns a pointer to the first Level object (currentLevel).
Level* loadLevels(std::vector<Level>& levels, ModelHandler* modelHandler) 
{
	FILE* file = fopen("levelsList", "r");

	int f;
	std::string tempString;

	fscanf(file, "Number of files: %d", &f);
	levels.reserve(f);
	for(int i = 1; i<=f; i++) 
	{
		char tempCString[51];
		fscanf(file, "%50s", tempCString);
		tempString = tempCString;
		levels.emplace_back(tempString, modelHandler);
	}

	fclose(file);
	file = nullptr;
	
	return &levels.front();
}

bool initGL()
{
	//Success flag
	bool success = true;

	// Enable culling
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

	return success;
}

/**
 * Initializes the InputHandler, WindowHandler and OpenGL
 */
bool init(bool& running) 
{

	if(!WindowHandler::getInstance().init()) 
	{
		running = false;
		return false;
	}

	SDL_WarpMouseInWindow(WindowHandler::getInstance().getWindow(), WindowHandler::getInstance().getScreenSize().x / 2.0f, WindowHandler::getInstance().getScreenSize().y / 2.0f);
	InputHandler::getInstance().init(&running);

	initGL();

	return true;
}

void updatePacman(Movable::direction dir, Camera* activeCamera, Pacman* pacman)
{
	if(activeCamera->isFirstPerson())
	{

		pacman->setDirectionFirstPerson(dir);
	}
	else
	{
		pacman->setDirection(dir);
	}
}

//While there are events in the eventQueue. Process those events. 
void update(float deltaTime, std::queue<GameEvent>& eventQueue, Camera*& activeCamera, std::vector<Camera>& cameras, Level* currentLevel, Pacman* pacman, ShaderHandler* shaderHandler, ShaderHandler::ShaderProgram*& currentShaderProgram) 
{
	GameEvent nextEvent;
	while(!eventQueue.empty()) 
	{
		nextEvent = eventQueue.front();
		eventQueue.pop();

		// Pacman movement direction
		if(nextEvent.action == ActionEnum::PLAYER_MOVE_RIGHT)
		{
			updatePacman(Movable::RIGHT, activeCamera, pacman);
		}
		else if(nextEvent.action == ActionEnum::PLAYER_MOVE_DOWN)
		{
			updatePacman(Movable::DOWN, activeCamera, pacman);
		}
		else if(nextEvent.action == ActionEnum::PLAYER_MOVE_UP)
		{
			updatePacman(Movable::UP, activeCamera, pacman);
		}
		else if(nextEvent.action == ActionEnum::PLAYER_MOVE_LEFT)
		{
			updatePacman(Movable::LEFT, activeCamera, pacman);
		}
		// Toggle between spotlight and pointlight at pacman
		else if(nextEvent.action == ActionEnum::PLAYER_SWITCH_LIGHT)
		{

			if(shaderHandler->getShader("multiLight")->programId == currentShaderProgram->programId)
			{
				currentShaderProgram = shaderHandler->getShader("flashLight");
			}
			else
			{
				currentShaderProgram = shaderHandler->getShader("multiLight");
			}
		}
		// Camera
		else if(nextEvent.action == ActionEnum::TOGGLE_PROJECTION)
		{
			activeCamera->togglePerspectiveView();
		}
		else if(nextEvent.action == ActionEnum::TOGGLE_CAMERA_VIEW)
		{
			for (auto &orb : *currentLevel->getOrbs())
			{
				orb.toggleScale();
			}
			for (auto &cherry : *currentLevel->getCherries())
			{
				cherry.toggleScale();
			}
			if (activeCamera == &cameras[0])
			{
				activeCamera = &cameras[1];
			}
			else
			{
				activeCamera = &cameras[0];
			}
			pacman->toggleFirstPerson();
		}
	}

	pacman->move(currentLevel, deltaTime);

	for (auto &orb : *currentLevel->getOrbs())
	{
		orb.update(deltaTime);
	}

	for (auto &cherry : *currentLevel->getCherries())
	{
		cherry.update(deltaTime);
	}

	currentLevel->updateGhosts(deltaTime, pacman->getPosition());
}


void renderScene(Level* currentLevel, Pacman* pacman, Camera* camera, ShaderHandler::ShaderProgram* shaderProgram, LightHandler* lightHandler) 
{
	for (auto &wall : *currentLevel->getWalls())
	{
		wall.draw(camera, shaderProgram, lightHandler);
	}

	for (auto &orb : *currentLevel->getOrbs())
	{
		orb.draw(camera, shaderProgram, lightHandler);
	}

	for (auto &cherry : *currentLevel->getCherries())
	{
		cherry.draw(camera, shaderProgram, lightHandler);
	}

	currentLevel->getGhosts()->draw(camera, shaderProgram, lightHandler);
	currentLevel->getFloor()->draw(camera, shaderProgram, lightHandler);

	for(GLuint i = 0; i < (*currentLevel->getPortals()).size(); ++i)
	{
		(*currentLevel->getPortals())[i].draw(camera, shaderProgram, lightHandler);
	}
}

void draw(Level* currentLevel, Pacman* pacman, Camera* camera, ShaderHandler::ShaderProgram* shaderProgram, LightHandler* lightHandler)
{
	lightHandler->setUpFBO();
	
	glClear(GL_DEPTH_BUFFER_BIT);

	renderScene(currentLevel, pacman, lightHandler->getCamera(), lightHandler->getShader(), lightHandler);

	for(GLuint i = 0; i < (*currentLevel->getPortals()).size(); ++i)
	{
		if (glm::distance(*(*currentLevel->getPortals())[i].getPosition(), *pacman->getPosition()) < 20.f)
		{
			(*currentLevel->getPortals())[i].setUpFBO();
	
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			renderScene(currentLevel, pacman, (*currentLevel->getPortals())[i].getCamera(), shaderProgram, lightHandler);
		}
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glViewport(0,0,WindowHandler::getInstance().getScreenSize().x,WindowHandler::getInstance().getScreenSize().y);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	renderScene(currentLevel, pacman, camera, shaderProgram, lightHandler);
	pacman->draw(camera, shaderProgram, lightHandler);
	// Update screen
	SDL_GL_SwapWindow(WindowHandler::getInstance().getWindow());
}

// Calls cleanup code on program exit.
void close() 
{
	WindowHandler::getInstance().close();
}

int main(int argc, char *argv[]) 
{
	float fpsGoal 		 = 60.0f;
	float nextFrame      = 1/fpsGoal; //Time between frames in seconds
	float nextFrameTimer = 0.0f; //Time from last frame in seconds
	float deltaTime      = 0.0f; //Time since last pass through of the game loop.
	auto clockStart      = std::chrono::high_resolution_clock::now(); //Clock used for timing purposes
	auto clockStop       = clockStart;
	bool running 		 = true;

	init(running);

	// Main event queue for the program.
	std::queue<GameEvent> eventQueue; 

	ModelHandler modelHandler;
	modelHandler.loadModelData("wall", "./resources/models/fancyWall/fancyWall.obj");
	modelHandler.loadModelData("floor", "./resources/models/floor/floor.obj");
	modelHandler.loadModelData("pacman", "./resources/models/pacman/pacman.obj");
	modelHandler.loadModelData("pacmanSantaHat", "./resources/models/pacmanSantaHat/pacmanSantaHat.obj");
	modelHandler.loadModelData("gift", "./resources/models/gift/gift.obj");
	modelHandler.loadModelData("santaHat", "./resources/models/santaHat/santaHat.obj");
	modelHandler.loadModelData("yellowGhost", "./resources/models/ghosts/yellowGhost.obj");
	modelHandler.loadModelData("blueGhost", "./resources/models/ghosts/blueGhost.obj");
	modelHandler.loadModelData("greenGhost", "./resources/models/ghosts/greenGhost.obj");
	modelHandler.loadModelData("redGhost", "./resources/models/ghosts/redGhost.obj");
	modelHandler.loadModelData("portal", "./resources/models/wall/wall.obj");

	ShaderHandler::ShaderProgram* currentShaderProgram = nullptr;
	ShaderHandler shaderHandler;
	currentShaderProgram = shaderHandler.initializeShaders();

	std::vector<Level> levels;
	Level* currentLevel = loadLevels(levels, &modelHandler);
	currentLevel->getGhosts()->setShader(shaderHandler.getShader("selfIlluminating"));

	Pacman pacman(modelHandler.getModel("pacman"), modelHandler.getModel("pacmanSantaHat"), currentLevel->getStartingPosition(), glm::vec3(1.0f));
	pacman.setShader(shaderHandler.getShader("selfIlluminating"));

	LightHandler lightHandler;
	lightHandler.setDirection(pacman.getRotationDirection());
	lightHandler.setPointLightAt(pacman.getPosition(), PACMAN, glm::vec3(1.0f, 1.0f, 0.2f));
	lightHandler.setPointLightAt(currentLevel->getGhosts()->getGhostPosition(RED), RED_GHOST, glm::vec3(1.0f, 0.2f, 0.2f));
	lightHandler.setPointLightAt(currentLevel->getGhosts()->getGhostPosition(BLUE), BLUE_GHOST, glm::vec3(0.2f, 0.2f, 1.0f));
	lightHandler.setPointLightAt(currentLevel->getGhosts()->getGhostPosition(GREEN), GREEN_GHOST, glm::vec3(0.2f, 1.0f, 0.2f));
	lightHandler.setPointLightAt(currentLevel->getGhosts()->getGhostPosition(YELLOW), YELLOW_GHOST, glm::vec3(1.0f, 1.0f, 0.2f));
	lightHandler.setSpotlightAt(pacman.getPosition());
	lightHandler.setShader(shaderHandler.getShader("shadow"));

	std::vector<Camera> cameras;
	cameras.reserve(2);
	cameras.emplace_back(currentLevel->getMapDimensions());
	cameras.emplace_back(pacman.getPosition(), pacman.getRotationDirection());

	Camera* activeCamera = &cameras[0];

	while(running) 
	{
		clockStart = std::chrono::high_resolution_clock::now();

		InputHandler::getInstance().readInput(eventQueue);
		update(deltaTime, eventQueue, activeCamera, cameras, currentLevel, &pacman, &shaderHandler, currentShaderProgram);

		if(nextFrameTimer >= nextFrame) 
		{
			draw(currentLevel, &pacman, activeCamera, currentShaderProgram, &lightHandler);
			nextFrameTimer = 0.0f;
		}

		clockStop = std::chrono::high_resolution_clock::now();
		deltaTime = std::chrono::duration<float, std::chrono::seconds::period>(clockStop - clockStart).count();
		
		nextFrameTimer += deltaTime;
	}

	close();
	return 0;
}