#include "Model.h"

Model::Model(const std::string &path)
{
	loadModel(path);
}

void Model::draw(Camera* const camera, ShaderHandler::ShaderProgram* const shaderProgram, LightHandler* const lightHandler, glm::mat4* const modelMatrix)
{
	for(auto &m : meshes_)
	{
		m.draw(camera, shaderProgram, lightHandler, modelMatrix);
	}
}

void Model::sendTextureId(GLuint texId)
{
	for(auto &m : meshes_)
	{
		m.setTextureId(texId);
	}
}

void Model::loadModel(const std::string &path)
{
	Assimp::Importer import;
	const aiScene* scene = import.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);

	if(!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
	{
		std::cout << "ERROR::ASSIMP::" << import.GetErrorString() << std::endl;
		return;
	}

	directory_ = path.substr(0, path.find_last_of('/'));

	processNode(scene->mRootNode, scene);
}

void Model::processNode(aiNode* node, const aiScene* scene)
{

	// Process all of the node's meshes (if there are any)
	for(size_t i = 0; i < node->mNumMeshes; ++i)
	{
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		meshes_.push_back(processMesh(mesh, scene));
	}

	// Do the same for all its children
	for(size_t i = 0; i < node->mNumChildren; ++i)
	{
		processNode(node->mChildren[i], scene);
	}
}

Mesh Model::processMesh(aiMesh* mesh, const aiScene* scene)
{
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;
	Texture texture;

	for(size_t i = 0; i < mesh->mNumVertices; ++i)
	{
		Vertex vertex;
		
		glm::vec3 tempVec;
		tempVec.x = mesh->mVertices[i].x;
		tempVec.y = mesh->mVertices[i].y;
		tempVec.z = mesh->mVertices[i].z;
		vertex.position = tempVec;

		tempVec.x = mesh->mNormals[i].x;
		tempVec.y = mesh->mNormals[i].y;
		tempVec.z = mesh->mNormals[i].z;
		vertex.normal = tempVec;

		if(mesh->mTextureCoords[0])
		{
			glm::vec2 vec;
			vec.x = mesh->mTextureCoords[0][i].x;
			vec.y = mesh->mTextureCoords[0][i].y;
			vertex.texCoords = vec;
		}
		else
		{
			vertex.texCoords = glm::vec2(0.0f, 0.0f);
		} 

		vertices.push_back(vertex);
	}
	
	for(size_t i = 0; i < mesh->mNumFaces; ++i)
	{
		aiFace face = mesh->mFaces[i];
		for(size_t j = 0; j < face.mNumIndices; ++j)
		{
			indices.push_back(face.mIndices[j]);
		}
	}

	if(mesh->mMaterialIndex >= 0)
	{
		aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

		texture = loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
	}

	return Mesh(vertices, indices, texture);
}

Texture Model::loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName)
{
	aiString str;
	mat->GetTexture(type, 0, &str);

	Texture texture;
	texture.id = textureFromFile(str.C_Str(), directory_);
	texture.type = typeName;
	texture.path = str;

	return texture;
}

GLint Model::textureFromFile(const char* path, std::string directory)
{
	std::string filename = std::string(path);
	filename = directory + '/' + filename;
	GLuint textureId;
	glGenTextures(1, &textureId);

	// SDL Image is used to load the image and save it as a surface.
	SDL_Surface* textureSurface = IMG_Load(path);

	if (!textureSurface)
	{
		printf("Failed to load texture \"%s\": %s\n", path, IMG_GetError());
		return 0;
	}

	// We check if the texture has an alpha channel.
	GLenum textureFormat = GL_RGB;
	if(textureSurface->format->BytesPerPixel == 4)
	{
		textureFormat = GL_RGBA;
	}

	// Handles cases where the system stores the texel data differently.
	GLenum texelFormat = getCorrectTexelFormat(textureSurface);

	glBindTexture(GL_TEXTURE_2D, textureId);
	glTexImage2D(GL_TEXTURE_2D, 0, textureFormat, textureSurface->w, textureSurface->h, 0, texelFormat, GL_UNSIGNED_BYTE, textureSurface->pixels);
	glGenerateMipmap(GL_TEXTURE_2D);

	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);
	SDL_FreeSurface(textureSurface);

	return textureId;
}

GLenum Model::getCorrectTexelFormat(const SDL_Surface *textureSurface)
{
	GLenum texelFormat = GL_RGBA;
	if (textureSurface->format->Bmask == 0xFF)
	{
		texelFormat = GL_BGR;
		if (textureSurface->format->BytesPerPixel == 4)
		{
			texelFormat = GL_BGRA;
		}
	}

	return texelFormat;
}