#include "Pacman.h"

Pacman::Pacman(Model* const model, Model* const model2, const glm::vec3& position, const glm::vec3& hitbox, const glm::vec3& scale, float rotation) 
	: Movable(model, position, hitbox, scale, rotation)
{
	pacModels_.push_back(*model);
	pacModels_.push_back(*model2);
	firstPerson_ =false;
	score_ = 0;
	activeCherry_ = false;
	cherryTimeLeft_ = 0.0;
	speed_ = 6.f;
} 

void Pacman::move(Level* const currentLevel, float deltaTime) 
{
	position_ = position_ + (movementDirection_ * deltaTime * speed_);

	handleCollision(currentLevel);

	if (movementDirection_ != rotationDirection_)
	{
		rotate(deltaTime);
	}
	cherryTimeLeft_ -= deltaTime;
	if (cherryTimeLeft_ < 0 && activeCherry_)
	{
		activeCherry_ = false;
		swapModels();
		speed_ = 6.f;
	}
	else if (cherryTimeLeft_ > 6.f)
	{
		speed_ += deltaTime * 5;
	}
}

void Pacman::draw(Camera* camera, ShaderHandler::ShaderProgram* shaderProgram, LightHandler* lightHandler)
{
	lightHandler->pointLightIndex = 0;
	Movable::draw(camera, shaderProgram, lightHandler);
}

void Pacman::swapModels()
{
	Model temp = pacModels_[0];
	pacModels_[0] = pacModels_[1];
	pacModels_[1] = temp;

	*model_ = pacModels_[0];
}

void Pacman::handleCollision(Level* const currentLevel)
{
	bool collision = false;


	for (size_t i = 0; i < currentLevel->getGhosts()->getGhosts()->size(); ++i)
	{
		if(checkCollisionWith(&(*currentLevel->getGhosts()->getGhosts())[i])) 
		{
			if (currentLevel->getGhosts()->isActive(i))
			{
				if (activeCherry_)
				{
					currentLevel->getGhosts()->die(i);
					score_ += 5;
				}
				else
				{
					position_ = currentLevel->getStartingPosition();
				}
			}
			
		}
	}

	for(auto &collidable : (*currentLevel->getWalls())) 
	{
		if(checkCollisionWith(&collidable)) 
		{
			if(firstPerson_)
			{
				resolveCollisionFirstPerson(currentLevel, &collidable);
			}
			else
			{
				resolveCollision(currentLevel, &collidable);
			}
			collision = true;
			break;
		}
	}

	if(collision) 
	{
		if(!firstPerson_)
		{
			setDirection(oldDirection_);
		}
	}

	oldDirection_ = Pacman::direction::NONE;
	oldMovementDirection_ = glm::vec3(0.0f, 0.0f, 0.0f);

	for (size_t i = 0; i < currentLevel->getOrbs()->size(); ++i)
	{
		if(checkCollisionWith(&(*currentLevel->getOrbs())[i])) 
		{
			(*currentLevel->getOrbs())[i] = currentLevel->getOrbs()->back();
			currentLevel->getOrbs()->pop_back();
			++score_;
			break;
		}
	}

	for (size_t i = 0; i < currentLevel->getCherries()->size(); ++i)
	{
		if(checkCollisionWith(&(*currentLevel->getCherries())[i])) 
		{
			(*currentLevel->getCherries())[i] = currentLevel->getCherries()->back();
			currentLevel->getCherries()->pop_back();
			if (!activeCherry_)
			{
				swapModels();
				activeCherry_ = true;
			}
			cherryTimeLeft_ = 8.f;
			break;
		}
	}

	for (auto &collidable : (*currentLevel->getPortals()))
	{
		if(checkCollisionWith(&collidable))
		{
			if(movementDirection_.x > 0.5f)
			{
				position_ -= glm::vec3(currentLevel->getMapDimensions().x * 2 - 5, 0.0f, 0.0f);
			}
			else if(movementDirection_.x < -0.5f)
			{
				position_ += glm::vec3(currentLevel->getMapDimensions().x * 2 - 5, 0.0f, 0.0f);
			}
			else if(movementDirection_.z > 0.5f)
			{
				position_ -= glm::vec3(0.0f, 0.0f, currentLevel->getMapDimensions().y * 2 - 5);
			}
			else if(movementDirection_.z < -0.5f)
			{
				position_ += glm::vec3(0.0f, 0.0f, currentLevel->getMapDimensions().y * 2 - 5);
			}
			break;
		}
	}
}

bool Pacman::checkCollisionWith(ScreenObject* const collidable)
{
	glm::vec3 collidablePos = *collidable->getPosition();
	glm::vec3 collidableBox = collidable->getHitbox();
	
	if(	(position_.x+hitbox_.x/2 <= collidablePos.x-collidableBox.x/2 || position_.x-hitbox_.x/2 >= collidablePos.x+collidableBox.x/2) ||
		(position_.z+hitbox_.z/2 <= collidablePos.z-collidableBox.z/2 || position_.z-hitbox_.z/2 >= collidablePos.z+collidableBox.z/2)) 
	{
		return false;
	}

	return true;
}

void Pacman::resolveCollision(Level* const currentLevel, ScreenObject* const collidable)
{
	glm::vec3 collidablePos = *collidable->getPosition();
	glm::vec3 collidableBox = collidable->getHitbox();
	
	glm::vec3 newPosition = position_;

	float col;
	float thi;

	switch(direction_) 
	{
		case(UP):
			col = (collidablePos.z+collidableBox.z/2);
			thi = (position_.z-hitbox_.z/2);
			newPosition.z = (position_.z + (col-thi));
			break;
		case(LEFT):
			col = (collidablePos.x+collidableBox.x/2);
			thi = (position_.x-hitbox_.x/2);
			newPosition.x = (position_.x + (col-thi));
			break;
		case(DOWN):
			col = (collidablePos.z-collidableBox.z/2);
			thi = (position_.z+hitbox_.z/2);
			newPosition.z = (position_.z - (thi-col));
			break;
		case(RIGHT):
			col = (collidablePos.x-collidableBox.x/2);
			thi = (position_.x+hitbox_.x/2);
			newPosition.x = (position_.x - (thi-col));
			break;
		case(NONE):
			//stuff
			break;
		default:
			printf("Error!: The direction is not valid.\n");
	}

	position_ = newPosition;
}

void Pacman::setDirection(Movable::direction direction) 
{
 	oldMovementDirection_ = movementDirection_;
	oldDirection_ = direction_;
 
 	switch(direction) 
 	{
 		case(UP):
			movementDirection_ = glm::vec3(0.0f, 0.0f, -1.0f);
 			break;
 		case(LEFT):
 			movementDirection_ = glm::vec3(-1.0f, 0.0f, 0.0f);
 			break;
 		case(DOWN):
			movementDirection_ = glm::vec3(0.0f, 0.0f, 1.0f);
 			break;
 		case(RIGHT):
 			movementDirection_ = glm::vec3(1.0f, 0.0f, 0.0f);
			break;
		case(NONE):
			movementDirection_ = glm::vec3(0.0f, 0.0f, 0.0f);
 			break;
		default:
			printf("Error!: The direction is not valid.\n");
	}
	direction_ = direction;
}

void Pacman::setDirectionFirstPerson(Movable::direction direction)
{
	oldMovementDirection_ = movementDirection_;
	oldDirection_ = direction_;

 	switch(direction) 
 	{
 		case(LEFT):
 			movementDirection_ = -glm::cross(movementDirection_, glm::vec3(0.0f, 1.0f, 0.0f));
 			break;
 		case(DOWN):
			movementDirection_ = -movementDirection_;
 			break;
 		case(RIGHT):
 			movementDirection_ = glm::cross(movementDirection_, glm::vec3(0.0f, 1.0f, 0.0f));
			break;
		case(NONE):
			
 			break;
		default:
			break;
	}

	if(movementDirection_.x > 0.5f)
	{
		direction_ = RIGHT;
	}
	else if(movementDirection_.x < -0.5f)
	{
		direction_ = LEFT;	
	}
	else if(movementDirection_.z > 0.5f)
	{
		direction_ = DOWN;
	}
	else if(movementDirection_.z < -0.5f)
	{
		direction_ = UP;
	}
}

void Pacman::resolveCollisionFirstPerson(Level* const currentLevel, ScreenObject* const collidable)
{
	glm::vec3 collidablePos = *collidable->getPosition();
	glm::vec3 collidableBox = collidable->getHitbox();
	
	glm::vec3 newPosition = position_;

	float col;
	float thi;

	if(movementDirection_.x > 0.5f)
	{
		col = (collidablePos.x-collidableBox.x/2);
		thi = (position_.x+hitbox_.x/2);
		newPosition.x = (position_.x - (thi-col)) - 0.001f;
	}
	else if(movementDirection_.x < -0.5f)
	{
		col = (collidablePos.x+collidableBox.x/2);
		thi = (position_.x-hitbox_.x/2);
		newPosition.x = (position_.x + (col-thi)) + 0.001f;
	}
	else if(movementDirection_.z > 0.5f)
	{
		col = (collidablePos.z-collidableBox.z/2);
		thi = (position_.z+hitbox_.z/2);
		newPosition.z = (position_.z - (thi-col)) - 0.001f;
	}
	else if(movementDirection_.z < -0.5f)
	{
		col = (collidablePos.z+collidableBox.z/2);
		thi = (position_.z-hitbox_.z/2);
		newPosition.z = (position_.z + (col-thi)) + 0.001f;
	}

	position_ = newPosition;
}