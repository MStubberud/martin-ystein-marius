#pragma once

#include <vector>

#include "ScreenObject.h"
#include "ModelHandler.h"
#include "LightHandler.h"
#include "ShaderHandler.h"
#include <iostream>

class Movable : public ScreenObject 
{
public:
	enum direction 
	{
		UP,
		LEFT,
		DOWN,
		RIGHT,
		NONE
	};

	Movable() {}; // For temp objects
	Movable(Model* const model, const glm::vec3& position, const glm::vec3& hitbox, const glm::vec3& scale = glm::vec3(1.0f), float rotation = 0.0f);

	void move(float deltaTime);
	void rotate(float deltaTime);
	void draw(Camera* camera, ShaderHandler::ShaderProgram* shaderProgram, LightHandler* lightHandler) override; 

	void setDirection(Movable::direction direction);

	void setSpeed(float speed) 								{ speed_ = speed; };
	void setShader(ShaderHandler::ShaderProgram* shader) 	{ shader_ = shader; };
	void setPosition(const glm::vec3& pos) 					{ position_ = pos; }

	glm::vec3* const getRotationDirection() 				{ return &rotationDirection_; };

protected:
	glm::vec3 movementDirection_ = glm::vec3(0.0f, 0.0f, 0.0f);
	glm::vec3 oldMovementDirection_ = glm::vec3(0.0f, 0.0f, 0.0f);
	direction direction_ = Movable::direction::NONE;
	direction oldDirection_ = Movable::direction::NONE;
	glm::vec3 rotationDirection_; 		// Current model forward

	glm::vec3 scale_;
	ShaderHandler::ShaderProgram* shader_;

	float rotation_;
	float rotationSpeed_;
	float speed_;
};