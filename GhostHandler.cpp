#include "GhostHandler.h"

void GhostHandler::init(const glm::vec2 &size, const glm::vec2 &pos, const glm::vec2 &doorDir, ModelHandler* modelHandler)
{
	tileSize = glm::vec3(size.x, 0.0f, size.y);

	startPos = glm::vec3(pos.x, 1.0f, pos.y);

	for (size_t i = 0; i < NUM_COLORS; ++i)
	{
		isDead[i] = false;
	}

	// Initializatin of ghosts
	Movable ghost(modelHandler->getModel("redGhost"), glm::vec3(pos.x, 0.9f, pos.y), glm::vec3(2.0f), glm::vec3(0.6f));
	ghosts.push_back(ghost);
	ghost = Movable(modelHandler->getModel("blueGhost"), glm::vec3(pos.x - doorDir.x * size.x, 0.9f, pos.y - doorDir.y * size.y), glm::vec3(2.0f), glm::vec3(0.6f));
	ghosts.push_back(ghost);
	ghost = Movable(modelHandler->getModel("greenGhost"), glm::vec3(pos.x + abs(glm::vec2(doorDir.y,doorDir.x)).x * size.x, 0.9f, pos.y + abs(glm::vec2(doorDir.y,doorDir.x)).y * size.y), glm::vec3(2.0f), glm::vec3(0.6f));
	ghosts.push_back(ghost);
	ghost = Movable(modelHandler->getModel("yellowGhost"), glm::vec3(pos.x + (abs(glm::vec2(doorDir.y,doorDir.x)) - doorDir).x * size.x, 0.9f, pos.y + (abs(glm::vec2(doorDir.y,doorDir.x)) - doorDir).y * size.y), glm::vec3(2.0f), glm::vec3(0.6f));
	ghosts.push_back(ghost);

	// Makes ghost start by leaving spawn
	moveDir[YELLOW] = glm::vec3(doorDir.x, 0.0f, doorDir.y);
	goalPos[YELLOW] = (*ghosts[YELLOW].getPosition()) + moveDir[YELLOW] * tileSize * 2.f;
	moveDir[BLUE] =  glm::vec3(doorDir.x, 0.0f, doorDir.y);
	goalPos[BLUE] = (*ghosts[BLUE].getPosition()) + moveDir[BLUE] * tileSize * 2.f;
	moveDir[RED] =  glm::vec3(doorDir.x, 0.0f, doorDir.y);
	goalPos[RED] = (*ghosts[RED].getPosition()) + moveDir[RED] *  tileSize * 2.f;
	moveDir[GREEN] =  glm::vec3(doorDir.x, 0.0f, doorDir.y);
	goalPos[GREEN] = (*ghosts[GREEN].getPosition()) + moveDir[GREEN] * tileSize * 2.f;
}


void GhostHandler::draw(Camera* camera, ShaderHandler::ShaderProgram* shaderProgram, LightHandler* lightHandler)
{
	for (size_t i = 0; i < ghosts.size(); ++i)
	{
		lightHandler->pointLightIndex = i + 1;
		ghosts[i].draw(camera, shaderProgram, lightHandler);
	}
}

void GhostHandler::setShader(ShaderHandler::ShaderProgram* const shader)
{
	for (auto &g : ghosts)
	{
		g.setShader(shader);
	}
}


void GhostHandler::update(float deltaTime, glm::vec3* pacmanPos, const std::vector<std::vector<int>>& map)
{
	for (size_t i = 0; i < ghosts.size(); ++i)
	{
		// Need new movement
		if (areClose(goalPos[i], (*ghosts[i].getPosition()))) // Check collision
		{
			// Calculate current tile and next
			int xTile = round((*ghosts[i].getPosition()).x / tileSize.x); 
			int zTile = round((*ghosts[i].getPosition()).z / tileSize.z);
			glm::vec3 end = glm::vec3(xTile, 0.0f ,zTile) + moveDir[i];
			if (map[end.z][end.x] != 7)
			{
				if (!isDead[i])
				{
					if (i == RED)
					{
						moveDir[i] = findTarget(map, end, xTile, zTile, i, pacmanPos);
					}
					else
					{
						moveDir[i] = simpleAI(map, end, xTile, zTile, i);
					}
				}
				else if (map[end.z][end.x] == 5)
				{
					isDead[i] = false;
				}
				else
				{
					moveDir[i] = findTarget(map, end, xTile, zTile, i, &startPos);
				}
			}
			else 
			{
				// At the edge of the map
				moveDir[i] = -moveDir[i];
			}
			ghosts[i].setPosition(goalPos[i]);
			goalPos[i] = (*ghosts[i].getPosition()) + moveDir[i] * tileSize;
		}
		ghosts[i].setDirection(findDirection(moveDir[i]));
		ghosts[i].move(deltaTime);
	}
}

Movable::direction GhostHandler::findDirection(const glm::vec3& moveDir)
{
	if (moveDir.x > 0.5f)
	{
		return Movable::direction::RIGHT;
	}
	else if(moveDir.x < -0.5f)
	{
		return Movable::direction::LEFT;
	}
	else if(moveDir.z < -0.5f)
	{
		return Movable::direction::UP;
	}
	else if(moveDir.z > 0.5f)
	{
		return Movable::direction::DOWN;
	}
	return Movable::direction::NONE;
}

bool GhostHandler::areClose(const glm::vec3 &pos1, const glm::vec3 &pos2)
{
	float threshold = 0.2f;
	return pos1.x - threshold <= pos2.x && pos1.x + threshold >= pos2.x
		&& pos1.z - threshold <= pos2.z && pos1.z + threshold >= pos2.z;
}

// Picks a random direction of the available ones, except opposite direction
glm::vec3 GhostHandler::simpleAI(const std::vector<std::vector<int>>& map, glm::vec3& end, const int& xTile, const int& zTile, int index)
{
	std::vector<glm::vec3> temp;
	if(map[end.z][end.x] != 1)
	{
		temp.push_back(glm::vec3(xTile, 0.0f, zTile) - end);
	}
	end = glm::vec3(xTile + moveDir[index].z, 0.0f ,zTile + moveDir[index].x);
	if(map[end.z][end.x] != 1)
	{
		temp.push_back(glm::vec3(xTile, 0.0f, zTile) - end);
	}
	end = glm::vec3(xTile - moveDir[index].z, 0.0f, zTile - moveDir[index].x);
	if(map[end.z][end.x] != 1)
	{
		temp.push_back(glm::vec3(xTile, 0.0f, zTile) - end);
	}
	if (temp.empty())
	{
		std::cout << "ERROR AI stuck" << std::endl;
	}
	std::uniform_int_distribution<int> distribution(0,temp.size() - 1);
	return -temp[distribution(generator)];
}

// Finds available tiles and sets movemet to the one closest to the target
glm::vec3 GhostHandler::findTarget(const std::vector<std::vector<int>>& map, glm::vec3& end, const int& xTile, const int& zTile,const int& index, glm::vec3* goal)
{
	std::vector<glm::vec3> temp;
	if(map[end.z][end.x] != 1)
	{
		temp.push_back(glm::vec3(xTile, 0.0f, zTile) - end);
	}
	end = glm::vec3(xTile + moveDir[index].z, 0.0f ,zTile + moveDir[index].x);
	if(map[end.z][end.x] != 1)
	{
		temp.push_back(glm::vec3(xTile, 0.0f, zTile) - end);
	}
	end = glm::vec3(xTile - moveDir[index].z, 0.0f, zTile - moveDir[index].x);
	if(map[end.z][end.x] != 1)
	{
		temp.push_back(glm::vec3(xTile, 0.0f, zTile) - end);
	}
	if (temp.empty())
	{
		std::cout << "ERROR AI stuck" << std::endl;
	}

	glm::vec3 tempDir;
	tempDir = temp.front();
	for (size_t i = 1; i < temp.size(); ++i)
	{
		glm::vec3 prevVec = (*ghosts[index].getPosition()) + tempDir - (*goal);
		float prevDist = sqrt(pow(prevVec.x, 2) + pow(prevVec.z,2));

		glm::vec3 newVec = (*ghosts[index].getPosition()) + temp[i] - (*goal);
		float newDist = sqrt(pow(newVec.x, 2) + pow(newVec.z,2));

		if (newDist > prevDist)
		{
			tempDir = temp[i];
		}
	}
	return -tempDir;
}

//// Finds available tiles and sets movemet to the one furthest away from the target
//glm::vec2 GhostHandler::fleeTarget(const float& dt, const std::vector<std::vector<int>>& map, glm::vec2& end, const int& xTile, const int& yTile,const int& index, const glm::vec2& goal)
//{
//	std::vector<glm::vec2> temp;
//	if(map[end.y + 1][end.x + 1] != 1)
//	{
//		temp.push_back(glm::vec2(xTile, yTile) - end);
//	}
//	end = glm::vec2(xTile + moveDir[index].y, yTile + moveDir[index].x);
//	if(map[end.y + 1][end.x + 1] != 1)
//	{
//		temp.push_back(glm::vec2(xTile, yTile) - end);
//	}
//	end = glm::vec2(xTile - moveDir[index].y, yTile - moveDir[index].x);
//	if(map[end.y + 1][end.x + 1] != 1)
//	{
//		temp.push_back(glm::vec2(xTile, yTile) - end);
//	}
//
//	glm::vec2 tempDir;
//	tempDir = temp.front();
//	for (size_t i = 1; i < temp.size(); ++i)
//	{
//		glm::vec2 prevVec = ghosts[index]->position + tempDir - goal;
//		float prevDist = sqrt(pow(prevVec.x, 2) + pow(prevVec.y,2));
//
//		glm::vec2 newVec = ghosts[index]->position + temp[i] - goal;
//		float newDist = sqrt(pow(newVec.x, 2) + pow(newVec.y,2));
//
//		if (newDist < prevDist)
//		{
//			tempDir = temp[i];
//		}
//	}
//	return -tempDir;
//}