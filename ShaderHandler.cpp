#include "ShaderHandler.h"

const std::string shaderPath = "./resources/shaders/";

ShaderHandler::ShaderProgram* ShaderHandler::initializeShaders()
{
	shaders["multiLight"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("multiLight"));
	shaders["selfIlluminating"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("selfIlluminating"));
	shaders["flashLight"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("flashLight"));
	shaders["shadow"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("shadow", true));

	return shaders["multiLight"].get(); 
}

ShaderHandler::ShaderProgram* ShaderHandler::getShader(const std::string& shader)
{
	auto it = shaders.find(shader);
	if (it != shaders.end())
	{
		return shaders[shader].get();
	}

	return nullptr;
}

ShaderHandler::ShaderProgram::ShaderProgram(const std::string& shader)
{
	const std::string vertexSuffix = ".vs";
	const std::string fragmentSuffix = ".fs";

	programId 		= LoadShaders((shaderPath + shader + vertexSuffix).c_str(), (shaderPath + shader + fragmentSuffix).c_str());
	
	MVPId 			= glGetUniformLocation(programId, "MVP");
	modelMatrixId 	= glGetUniformLocation(programId, "modelMatrix");

	textureId 	= glGetUniformLocation(programId, "material.texture");
	shininessId = glGetUniformLocation(programId, "material.shininess");
	
	cubeMapId = glGetUniformLocation(programId, "cubeMap");

	for(size_t i = 0; i < 6; ++i)
	{
		shadowMatricesIds[i] = glGetUniformLocation(programId, ("shadowMatrices[" + std::to_string(i) + "]").c_str());
	}

	pacmanLightPositionId 		= glGetUniformLocation(programId, "pointLights[0].position");
	pacmanLightConstantId 		= glGetUniformLocation(programId, "pointLights[0].constant");
	pacmanLightLinearId 		= glGetUniformLocation(programId, "pointLights[0].linear");
	pacmanLightQuadraticId 		= glGetUniformLocation(programId, "pointLights[0].quadratic");
	pacmanLightDiffuseId 		= glGetUniformLocation(programId, "pointLights[0].diffuse");
	pacmanLightSpecularId 		= glGetUniformLocation(programId, "pointLights[0].specular");

	redGhostLightPositionId 	= glGetUniformLocation(programId, "pointLights[1].position");
	redGhostLightConstantId 	= glGetUniformLocation(programId, "pointLights[1].constant");
	redGhostLightLinearId 		= glGetUniformLocation(programId, "pointLights[1].linear");
	redGhostLightQuadraticId 	= glGetUniformLocation(programId, "pointLights[1].quadratic");
	redGhostLightDiffuseId 		= glGetUniformLocation(programId, "pointLights[1].diffuse");
	redGhostLightSpecularId 	= glGetUniformLocation(programId, "pointLights[1].specular");

	blueGhostLightPositionId 	= glGetUniformLocation(programId, "pointLights[2].position");
	blueGhostLightConstantId 	= glGetUniformLocation(programId, "pointLights[2].constant");
	blueGhostLightLinearId 		= glGetUniformLocation(programId, "pointLights[2].linear");
	blueGhostLightQuadraticId 	= glGetUniformLocation(programId, "pointLights[2].quadratic");
	blueGhostLightDiffuseId 	= glGetUniformLocation(programId, "pointLights[2].diffuse");
	blueGhostLightSpecularId 	= glGetUniformLocation(programId, "pointLights[2].specular");

	greenGhostLightPositionId 	= glGetUniformLocation(programId, "pointLights[3].position");
	greenGhostLightConstantId 	= glGetUniformLocation(programId, "pointLights[3].constant");
	greenGhostLightLinearId 	= glGetUniformLocation(programId, "pointLights[3].linear");
	greenGhostLightQuadraticId 	= glGetUniformLocation(programId, "pointLights[3].quadratic");
	greenGhostLightDiffuseId 	= glGetUniformLocation(programId, "pointLights[3].diffuse");
	greenGhostLightSpecularId 	= glGetUniformLocation(programId, "pointLights[3].specular");

	yellowGhostLightPositionId 	= glGetUniformLocation(programId, "pointLights[4].position");
	yellowGhostLightConstantId 	= glGetUniformLocation(programId, "pointLights[4].constant");
	yellowGhostLightLinearId 	= glGetUniformLocation(programId, "pointLights[4].linear");
	yellowGhostLightQuadraticId = glGetUniformLocation(programId, "pointLights[4].quadratic");
	yellowGhostLightDiffuseId 	= glGetUniformLocation(programId, "pointLights[4].diffuse");
	yellowGhostLightSpecularId 	= glGetUniformLocation(programId, "pointLights[4].specular");

	spotLightPositionId 	= glGetUniformLocation(programId, "spotLight.position");
	spotLightDirectionId 	= glGetUniformLocation(programId, "spotLight.direction");
	spotLightCutOffId 		= glGetUniformLocation(programId, "spotLight.cutOff");
	spotLightOuterCutOffId 	= glGetUniformLocation(programId, "spotLight.outerCutOff");
	spotLightConstantId 	= glGetUniformLocation(programId, "spotLight.constant");
	spotLightLinearId 		= glGetUniformLocation(programId, "spotLight.linear");
	spotLightQuadraticId 	= glGetUniformLocation(programId, "spotLight.quadratic");
	spotLightDiffuseId 		= glGetUniformLocation(programId, "spotLight.diffuse");
	spotLightSpecularId 	= glGetUniformLocation(programId, "spotLight.specular");

	cameraPositionId 	= glGetUniformLocation(programId, "cameraPos");
	lightPositionId		= glGetUniformLocation(programId, "lightPos");
	pointLightIndexId	= glGetUniformLocation(programId, "pointLightIndex");

	glUseProgram(programId);
}

ShaderHandler::ShaderProgram::ShaderProgram(const std::string& shader, bool t)
{
	const std::string vertexSuffix = ".vs";
	const std::string fragmentSuffix = ".fs";
	const std::string geometrySuffix = ".gs";

	programId 		= LoadShaders((shaderPath + shader + vertexSuffix).c_str(), (shaderPath + shader + fragmentSuffix).c_str(), (shaderPath + shader + geometrySuffix).c_str());
	
	MVPId 			= glGetUniformLocation(programId, "MVP");
	modelMatrixId 	= glGetUniformLocation(programId, "modelMatrix");

	textureId 	= glGetUniformLocation(programId, "material.texture");
	shininessId = glGetUniformLocation(programId, "material.shininess");
	
	cubeMapId = glGetUniformLocation(programId, "cubeMap");
	
	for(size_t i = 0; i < 6; ++i)
	{
		shadowMatricesIds[i] = glGetUniformLocation(programId, ("shadowMatrices[" + std::to_string(i) + "]").c_str());
	}

	pacmanLightPositionId 		= glGetUniformLocation(programId, "pointLights[0].position");
	pacmanLightConstantId 		= glGetUniformLocation(programId, "pointLights[0].constant");
	pacmanLightLinearId 		= glGetUniformLocation(programId, "pointLights[0].linear");
	pacmanLightQuadraticId 		= glGetUniformLocation(programId, "pointLights[0].quadratic");
	pacmanLightDiffuseId 		= glGetUniformLocation(programId, "pointLights[0].diffuse");
	pacmanLightSpecularId 		= glGetUniformLocation(programId, "pointLights[0].specular");

	redGhostLightPositionId 	= glGetUniformLocation(programId, "pointLights[1].position");
	redGhostLightConstantId 	= glGetUniformLocation(programId, "pointLights[1].constant");
	redGhostLightLinearId 		= glGetUniformLocation(programId, "pointLights[1].linear");
	redGhostLightQuadraticId 	= glGetUniformLocation(programId, "pointLights[1].quadratic");
	redGhostLightDiffuseId 		= glGetUniformLocation(programId, "pointLights[1].diffuse");
	redGhostLightSpecularId 	= glGetUniformLocation(programId, "pointLights[1].specular");

	blueGhostLightPositionId 	= glGetUniformLocation(programId, "pointLights[2].position");
	blueGhostLightConstantId 	= glGetUniformLocation(programId, "pointLights[2].constant");
	blueGhostLightLinearId 		= glGetUniformLocation(programId, "pointLights[2].linear");
	blueGhostLightQuadraticId 	= glGetUniformLocation(programId, "pointLights[2].quadratic");
	blueGhostLightDiffuseId 	= glGetUniformLocation(programId, "pointLights[2].diffuse");
	blueGhostLightSpecularId 	= glGetUniformLocation(programId, "pointLights[2].specular");

	greenGhostLightPositionId 	= glGetUniformLocation(programId, "pointLights[3].position");
	greenGhostLightConstantId 	= glGetUniformLocation(programId, "pointLights[3].constant");
	greenGhostLightLinearId 	= glGetUniformLocation(programId, "pointLights[3].linear");
	greenGhostLightQuadraticId 	= glGetUniformLocation(programId, "pointLights[3].quadratic");
	greenGhostLightDiffuseId 	= glGetUniformLocation(programId, "pointLights[3].diffuse");
	greenGhostLightSpecularId 	= glGetUniformLocation(programId, "pointLights[3].specular");

	yellowGhostLightPositionId 	= glGetUniformLocation(programId, "pointLights[4].position");
	yellowGhostLightConstantId 	= glGetUniformLocation(programId, "pointLights[4].constant");
	yellowGhostLightLinearId 	= glGetUniformLocation(programId, "pointLights[4].linear");
	yellowGhostLightQuadraticId = glGetUniformLocation(programId, "pointLights[4].quadratic");
	yellowGhostLightDiffuseId 	= glGetUniformLocation(programId, "pointLights[4].diffuse");
	yellowGhostLightSpecularId 	= glGetUniformLocation(programId, "pointLights[4].specular");

	spotLightPositionId 	= glGetUniformLocation(programId, "spotLight.position");
	spotLightDirectionId 	= glGetUniformLocation(programId, "spotLight.direction");
	spotLightCutOffId 		= glGetUniformLocation(programId, "spotLight.cutOff");
	spotLightOuterCutOffId 	= glGetUniformLocation(programId, "spotLight.outerCutOff");
	spotLightConstantId 	= glGetUniformLocation(programId, "spotLight.constant");
	spotLightLinearId 		= glGetUniformLocation(programId, "spotLight.linear");
	spotLightQuadraticId 	= glGetUniformLocation(programId, "spotLight.quadratic");
	spotLightDiffuseId 		= glGetUniformLocation(programId, "spotLight.diffuse");
	spotLightSpecularId 	= glGetUniformLocation(programId, "spotLight.specular");

	cameraPositionId 	= glGetUniformLocation(programId, "cameraPos");
	lightPositionId		= glGetUniformLocation(programId, "lightPos");
	pointLightIndexId	= glGetUniformLocation(programId, "pointLightIndex");

	glUseProgram(programId);
}

ShaderHandler::ShaderProgram::~ShaderProgram()
{
	glDeleteProgram(programId);
}